YouTube TV Unofficial Release!
===================

This is an unofficial release of [YouTube TV](https://youtube.com/tv/) for Google Chrome users so you can use it as a Chrome Application. Enjoy! 

This extension was made using [WebDGap](https://michaelsboost.github.io/WebDGap/).

You can now install [YouTube TV ](https://chrome.google.com/webstore/detail/ihjglecocookcboibmijdlpopamlcclm) from the Chrome Web Store.
